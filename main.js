new Vue({
    el: '#app',
    vuetify: new Vuetify(),
    data: {
		id_cust: 1,
		items: [],
		carts: [],
		unit:0,
		stock_temp: [],
        id_item: '',
		dialogBuy: false,
		dialogAddCart: false,
        buyItem: '',
		header_notif: "",
		body_notif: "",
		dialogNotif: false
    },
    created: function () {
//axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
		//axios.defaults.headers.common['Content-Type'] = 'application/x-www-form-urlencoded'
		//axios.defaults.headers.common['Accept'] = 'application/json, text/plain, */*'
		//axios.defaults.headers.common['Access-Control-Allow-Headers'] = 'Origin, Accept, Content-Type, Authorization, Access-Control-Allow-Origin'
        this.getItems()
		this.getItemCarts()
    },
    methods: {

		
        // Get item
        getItems: function () {
			
            axios.get('http://localhost:8000/master/item', {
			
			
			})
			.then(res => {
				this.items = res.data.data;
			})
			.catch(err => {
				// handle error
				console.log(err);
			})
        },
		
		// Get item cart
        getItemCarts: function () {
			
            axios.get('http://localhost:8000/trans/sales/cart', {
			
			
			})
			.then(res => {
				this.carts = res.data.data;
			})
			.catch(err => {
				// handle error
				console.log(err);
			})
        },

        // buy item
        buyItems: function () {
            axios.post('http://localhost:8000/trans/sales', {
                    id_cust: this.id_cust,
                })
			.then(res => {
				// handle success
				this.getItems();
				this.id_cust = '';
				this.id_item = '';
				this.dialogBuy = false;
			})
			.catch(err => {
				// handle error
				console.log(err);
			})
        },
		
		showDialogAddCart: function (id) {
			this.unit = 0;
			this.id_item = id;
            this.dialogAddCart = true;
        },
		
		// add bucket item
        addCart: function () {
            axios.post('http://localhost:8000/trans/sales/cart/add', {
                    id_cust: this.id_cust,
					id_item: this.id_item,
					unit: this.unit,
                })
			.then(res => {
				// handle success
				this.getItems();
				this.getItemCarts();
				this.id_item = '';
				this.dialogAddCart = false;
			})
			.catch(err => {
				// handle error
				console.log(err);
			})
        },
		
		
		
		// add bucket item
        removeCart: function (id) {
			axios.post('http://localhost:8000/trans/sales/cart/remove', {
                    id_cust: this.id_cust,
					id_item: id,
                })
			.then(res => {
				// handle success
				this.getItems();
				this.getItemCarts();
				this.id_item = '';
			})
			.catch(err => {
				// handle error
				console.log(err);
			})
        },
    }
})